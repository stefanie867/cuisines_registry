package de.quandoo.recruitment.registry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {
    
    Multimap<Cuisine, Customer> registryByCuisine = ArrayListMultimap.create();
    Multimap<Customer, Cuisine> registryByCustomer = ArrayListMultimap.create();
    
    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
    	if (isSupportedCuisine(cuisine)) {
	    	registryByCuisine.put(cuisine, customer);
	    	registryByCustomer.put(customer, cuisine);
    	} else {
    		System.err.println("Unknown cuisine, please reach johny@bookthattable.de to update the code");
    	}
    }

	private boolean isSupportedCuisine(Cuisine cuisine) {
		String cuisineName = cuisine.getName();
		return cuisineName.equals("german") || cuisineName.equals("french") || cuisineName.equals("italian");
	}

	@Override
    public List<Customer> getCustomersOf(final Cuisine cuisine) {
    	return (List<Customer>) registryByCuisine.get(cuisine);
    }

    @Override
    public List<Cuisine> getCuisinesOf(final Customer customer) {	    	
    	return (List<Cuisine>) registryByCustomer.get(customer);
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
    	Map<Integer, Cuisine> cuisines = new TreeMap<>(Collections.reverseOrder());
    	
    	for(Cuisine cuisine : registryByCuisine.keySet()) {
    		cuisines.put(registryByCuisine.get(cuisine).size(), cuisine);
    	}
    	
    	return getTopN(cuisines, n);
    }

	private List<Cuisine> getTopN(Map<Integer, Cuisine> cuisines, int n) {
		List<Cuisine> result = new ArrayList<>();
		
		int top = 0;
		
    	for(Integer key : cuisines.keySet()) {
    		result.add(cuisines.get(key));
    		top++;
    		if (top == n ) {
    			break;
    		}
    	}    	
    	
    	return result;    	
	}
}
