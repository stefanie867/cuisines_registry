package de.quandoo.recruitment.registry.model;

import java.util.Objects;

public class Cuisine {
    private final String name;

    private Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public static Cuisine of(String name) {
    	return new Cuisine(name);
    }

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Cuisine other = (Cuisine) obj;
		return Objects.equals(this.getName(), other.getName());
	}
    
    

}
