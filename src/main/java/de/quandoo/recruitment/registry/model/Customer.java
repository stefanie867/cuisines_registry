package de.quandoo.recruitment.registry.model;

import java.util.Objects;

public class Customer {
    private final String customerId;

    private Customer(final String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }
    
    public static Customer of(String customerId) {
    	return new Customer(customerId);
    }

	@Override
	public int hashCode() {
		return Objects.hash(customerId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Customer other = (Customer) obj;
		return Objects.equals(this.customerId, other.customerId);
	}
    
}
