package de.quandoo.recruitment.registry;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry;

    @Before
    public void init() {
    	cuisinesRegistry = new InMemoryCuisinesRegistry();
    	
    	cuisinesRegistry.register(Customer.of("1"), Cuisine.of("french"));
        cuisinesRegistry.register(Customer.of("2"), Cuisine.of("german"));
        cuisinesRegistry.register(Customer.of("3"), Cuisine.of("italian"));
        cuisinesRegistry.register(Customer.of("4"), Cuisine.of("italian"));
        cuisinesRegistry.register(Customer.of("4"), Cuisine.of("german"));
        cuisinesRegistry.register(Customer.of("5"), Cuisine.of("german"));
    }

    @Test
    public void ifCuisineNullThenReturnEmptyCustomerList() {
        List<Customer> result = cuisinesRegistry.getCustomersOf(null);
        assert(result.size() == 0);
    }

    @Test
    public void ifCustomerNullThenReturnEmptyCuisineList() {
        List<Cuisine> result = cuisinesRegistry.getCuisinesOf(null);
        assert(result.size() == 0);
    }
    
    @Test
    public void ifCuisineFrenchThenReturnCustomer1() {
        List<Customer> result = cuisinesRegistry.getCustomersOf(Cuisine.of("french"));
        assert(result.get(0).equals(Customer.of("1")));
    }
    
    @Test
    public void ifCustomer1ThenReturnFrench() {
    	List<Cuisine> result = cuisinesRegistry.getCuisinesOf(Customer.of("1"));
    	assert(result.get(0).equals(Cuisine.of("french")));
    }
    
    @Test
    public void ifCustomer4ThenReturn2Cuisines() {
    	List<Cuisine> result = cuisinesRegistry.getCuisinesOf(Customer.of("4"));
    	assert(result.size() == 2);
    }

    @Test
    public void ifTop2ThenReturnGermanAndItalian() {
        List<Cuisine> result = cuisinesRegistry.topCuisines(2);
        assert(
    		result.get(0).equals(Cuisine.of("german")) && 
    		result.get(1).equals(Cuisine.of("italian"))
		);
    }
    
    @Test
    public void ifTop1ThenReturnGerman() {
        List<Cuisine> result = cuisinesRegistry.topCuisines(1);
        assert(result.get(0).equals(Cuisine.of("german")));
    }


}